//""Ponemos a correr el servidor
const { request, response } = require("express");
const express = require("express");
const app = express();
const port = 3001 
app.listen(port);
console.log(`Server is running on port ${port}`)

// console.log ("hola");
// emepzamos a declarar los metodos
app.get('/', (request, response) =>{
    response.send('<h1>PAGINA PRINCIPAL</h1>');
    console.log("estamos en la ruta principal del sitio")
})
app.get('/', (request, response) =>{
    response.send('<h2>PRODUCTOS</h2>');
})

let productos =[
    {'id':1, 'nombre':'Celular'},        //posicion 0
    {'id':3, 'nombre':'Notebook'},       //posicion 1
    {'id':4, 'nombre':'Play Station'},   //posicion 2
    {'id':2, 'nombre':'Impresora'},      //posicion 3
    {'id':5, 'nombre':'Monitor'},        //posicion 4
]
app.get('/productos', (request, response) =>{
    response.send(productos);
    console.log(productos);
})

app.get('/productos/:parametro', (request, response)=>{
const p = Number (request.params.parametro);
// console.log(productos[id].nombre);  // aqui estoy accediendo al elemto segun ubicacion
// response.send('producto segun subindice:' + productos[id].nombre);

const producto = productos.find(objeto => objeto.id === p);  //busca el elemto cuyo id se igual al parametro
console.log(producto);
response.send(`${JSON.stringify(producto)} - ${producto.id} - ${producto.nombre}`);
})

app.delete('/productos/:parametro', (request, response) =>{
    // response.send("Habilitado el delete");
    const p = Number (request.params.parametro);
    productos = productos.filter(objeto => objeto.id !==p);
    console.log (productos);
    response.send(productos);
})
app.post('/productos', (request, response) =>{
response.send("Habilitado el post");
const producto = request.body;
console.log(producto);
})