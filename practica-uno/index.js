const express = require('express');
const app = express();
app.use(express.json());

const port = 3500;
app.listen(port);
console.log(`Server is running on port ${port}`)

app.get('/', (request, response) => {
    response.json(`<h1>Pagina Principal </h1>`);
})

app.get('/productos/:id', (request, response) => {
    const id = Number(request.params.id);
    const producto = productos.find(produ => produ.id === id);
    productos = productos.filter(produ => produ.id !== id);
    response.json(producto);
})


let productos = [
    {
        "id": 1,
        "nombre": "notebook Dell 3500",
        "valor": 100000
    },
    {
        "id": 2,
        "nombre": "Mouse logitech",
        "valor": 4000
    }
]
app.get('/productos', (request, response) => {
    response.json(productos);
})

app.post('/productos', (request, response) => {
    // response.json("estoy en post");
    const producto = request.body;
    response.json(producto);
    productos.push(producto);
})

app.delete('/productos/:id', (request, response) => {
    const id = Number(request.params.id);
    const producto = productos.find(produ => produ.id === id);
    productos = productos.filter(produ => produ.id !== id);
    response.json(producto);
})
app.patch('/productos/:id', (request, response) => {
    const id = Number(request.params.id);
    const indice = productos.findIndex(produ => produ.id === id);
    const producto = request.body;
    response.json(producto);
    productos[indice] = producto;
})