// fetch("https://tecnologia-693f2-default-rtdb.firebaseio.com/productos.json/")
// .then(response => response.json())
// .then(data => console.log(data));
const HTTPResponse = document.querySelector('#elemento');
// fetch("https://tecnologia-693f2-default-rtdb.firebaseio.com/productos.json/")
// .then(response => response.json())
// .then(productos => {
//     const tpl = productos.map((prod) => `<li>${prod.id} - ${prod.nombre} - ${prod.valor}</li>`)
//     HTTPResponse.innerHTML = `<ul>${tpl}</ul>`
// });
const ul = document.createElement('ul');

fetch("https://tecnologia-4c617-default-rtdb.firebaseio.com/productos.json/")
.then(response => response.json())
.then(productos => {
    productos.forEach(prod => {
        let elem = document.createElement('li');
        elem.appendChild(document.createTextNode(`${prod.id} - ${prod.nombre} - ${prod.valor}`));
        ul.appendChild(elem);
    });
    HTTPResponse.appendChild(ul);
});